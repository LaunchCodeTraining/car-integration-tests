package org.launchcode.devops.carintegrationtesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarIntegrationTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarIntegrationTestingApplication.class, args);
	}

}
