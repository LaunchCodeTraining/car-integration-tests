package org.launchcode.devops.carintegrationtesting.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.launchcode.devops.carintegrationtesting.data.CarRepository;
import org.launchcode.devops.carintegrationtesting.models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping(CarsController.ROOT_PATH)
public class CarsController {
  public static final String ROOT_PATH = "/cars";

  @Autowired
  private CarRepository carRepository;
  
  @GetMapping
  public ResponseEntity<List<Car>> getCarsCollection() {
    return ResponseEntity.ok(carRepository.findAll());
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity getCarsById(@PathVariable Long id) {
    Optional<Car> optionalCar = carRepository.findById(id);
    if(optionalCar.isPresent()) {
      return ResponseEntity.ok(optionalCar.get());
    }
    return ResponseEntity.notFound().build();
  }

  @PostMapping
  public ResponseEntity<Car> createCar(@RequestBody Car partialCar) {
    Car newCar = carRepository.save(partialCar);
    return ResponseEntity.created(URI.create(ROOT_PATH)).body(newCar);
  }
   
}
