package org.launchcode.devops.carintegrationtesting.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cars")
public class Car {

  @Id 
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private long id;
  private String make;
  private String model;
  private int gasTankSize;
  private double gasTankLevel;
  private double milesPerGallon;
  private double odometer;
  private double tripMeter;


  public Car(String make, String model, int gasTankSize, double milesPerGallon) {
      this.make = make;
      this.model = model;
      this.gasTankSize = gasTankSize;
      // Gas tank level defaults to a full tank
      this.gasTankLevel = gasTankSize;
      this.milesPerGallon = milesPerGallon;
      this.odometer = 0;
      this.tripMeter = 0;
  }

  public Car() {}

  public Long getId() {
    return id;
  }

  public String getMake() {
      return make;
  }

  public void setMake(String make) {
      this.make = make;
  }

  public String getModel() {
      return model;
  }

  public void setModel(String model) {
      this.model = model;
  }

  public int getGasTankSize() {
      return gasTankSize;
  }

  public void setGasTankSize(int gasTankSize) {
      this.gasTankSize = gasTankSize;
  }

  public double getGasTankLevel() {
      return gasTankLevel;
  }

  public void setGasTankLevel(double gasTankLevel) {
      this.gasTankLevel = gasTankLevel;
  }

  public double getMilesPerGallon() {
      return milesPerGallon;
  }

  public void setMilesPerGallon(double milesPerGallon) {
      this.milesPerGallon = milesPerGallon;
  }

  public double getOdometer() {
      return odometer;
  }

  /**
   * Drive the car an amount of miles. If not enough fuel, drive as far as fuel allows.
   * Adjust fuel levels based on amount needed to drive the distance requested.
   * Add miles to odometer.
   *
   * @param miles - the miles to drive
   */
  public void drive(double miles)
  {
      //adjust fuel based on mpg and miles requested to drive
      double maxDistance = this.milesPerGallon * this.gasTankLevel;
      double milesAbleToTravel = miles > maxDistance ? maxDistance : miles;
      double gallonsUsed = milesAbleToTravel / this.milesPerGallon;
      this.gasTankLevel = this.gasTankLevel - gallonsUsed;
      this.odometer += milesAbleToTravel;
      this.tripMeter += milesAbleToTravel;

  }

  public void addGas(double amount) {
      if(this.gasTankLevel + amount > this.gasTankSize) {
          throw new IllegalArgumentException("Cannot exceed gas tank level");
      }
      this.gasTankLevel += amount;
  }

  public double range() {
      return this.gasTankLevel * this.milesPerGallon;
  }

  public double getTripMeter() {
      return this.tripMeter;
  }

  public void resetTripMeter() {
      this.tripMeter = 0;
  }

}
