package org.launchcode.devops.carintegrationtesting.data;

import org.launchcode.devops.carintegrationtesting.models.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {}
